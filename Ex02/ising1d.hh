#ifndef NQS_ISING1D_HH
#define NQS_ISING1D_HH

#include <iostream>
#include <Eigen/Dense>
#include <random>
#include <complex>
#include <vector>

namespace nqs{

using namespace std;
using namespace Eigen;

class Ising1d{

  const int nspins_;
  double J_;
  double h_;
  
public:

  Ising1d(int nspins,double h,double J=1):nspins_(nspins),h_(h),J_(J){
  }


  void FindConn(const VectorXd & v,vector<double> & mel,vector<vector<int> > & connectors){

    connectors.clear();
    connectors.resize(nspins_+1);
    mel.resize(nspins_+1);

    mel[0]=0;
    connectors[0].resize(0);

    for(int i=0;i<nspins_;i++){
      mel[i+1]=-h_;
      connectors[i+1].push_back(i);

      mel[0]-=J_*(2*v(i)-1.)*(2.*v((i+1)%nspins_)-1.);
    }

  }

  MatrixXi PermutationTable(){
    MatrixXi permtable(nspins_,nspins_);

    for(int i=0;i<nspins_;i++){
      for(int p=0;p<nspins_;p++){
        permtable(i,p)=(i+p)%nspins_;
      }
    }
    return permtable;
  }

};


}

#endif
