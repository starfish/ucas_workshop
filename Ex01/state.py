class State:
    def __init__(self,p1):
        self.p1=p1

    def __call__(self,x):
        return np.exp(self.p1*x**2.)

    #Compute the logarithm of the wave-function
    def LogVal(self,x):
        return self.p1*x**2.

    #Set the variational parameter
    def SetParam(self,p1):
        self.p1=p1

    #Get the variational parameter
    def GetParam(self):
        return self.p1

    #Computes the Derivative of LogPsi with
    #srespect to the variational parameter
    def DerLog(self,x):
        return x**2.

    #Computes (Nabla^2*Psi)/Psi
    #Needed in the local energy
    def Nabla2OPsi(self,x):
        return 2.*self.p1+(2.*self.p1*x)**2.
